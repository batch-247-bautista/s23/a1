

let person = {
			name: 'Ash Ketchum',
			age: '10',
			friends: {
				Kanto: ['Brock','Misty'],
				Hoenn: ['Max','May'],
			},
			Pokemon: ['Picachu',
                   'Charizard',
                   'Squirtle',
                   'Bulbasaur'],
			
			talk: function(){

				console.log('Picachu, I choose you!');
			}
		};
		
		console.log(person);
		function Player(name, birthDate, category){
			this.name = name;
			this.birthDate = birthDate;
			this.category = category;
		};

		let myPlayer = new Player('Ash Ketchum', 1990, 'Expert');
		//console.log('Result from creating objects using object constructors:');
		//console.log(Player);




		console.log('Result from dot notation: ');
		console.log('' + myPlayer.name);
		//console.log(+ pokemon['pokemon']);
		//console.log('Result from object methods:');
		//person.talk();

		
		
		
				




		let pokemonArray = [ 
					'Picachu',
                   'Charizard',
                   'Squirtle',
                   'Bulbasaur'];
				
		let array = [pokemonArray];

		console.log('Result of square bracket notation:');
		console.log(array[0]);
		
		
		
		console.log('Result of talk method:');
		person.talk();


		

			
		let myPokemon = [
				{
					name: 'Pikachu',
					level: 12,
					health: 24,
					attack: 12,
					tackle: function(){
					console.log('This Pokemon tackled targetPokemon');
					console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
				},
					faint: function(){
					console.log(myPokemon);
				}
				},
				{
					name: 'Geodude',
					level: 8,
					health: 16,
					attack: 8,
					tackle: function(){
					console.log('This Pokemon tackled targetPokemon');
					
				},
					faint: function(){
					cconsole.log(myPokemon);
				}
				},
				{
					name: 'Mewteo',
					level: 100,
					health: 200,
					attack: 100,
					tackle: function(){
					console.log('This Pokemon tackled targetPokemon');
					console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
				},
					faint: function(){
					console.log(myPokemon);
				}
				}
				];

				//console.log('Result from dot notation: ' + PokemonArray['name']);
				console.log(myPokemon);

		
		function Pokemon(name, level){

			// Properties
			this.name = name;
			this.level = level;
			this.health = 2 * level;
			this.attack = level;

			// Methods
			this.tackle = function(target){
				console.log(this.name + ' tackled ' + target.name);
				console.log("Picachu's health is now reduced to " + Number(target.health - this.attack));
			};

			

		}
			let geodude = new Pokemon('Geodude', 0);
			let picachu = new Pokemon('Picachu', 8);

			geodude.tackle(picachu);
				
		
		let newPokemon = [
				{
					name: 'Pikachu',
					level: 12,
					health: 16,
					attack: 12,
					tackle: function(){
					console.log('This Pokemon tackled targetPokemon');
					console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
				},
					faint: function(){
					console.log('Pokemon fainted');
				}
			}
			];
			
		function Pokemon(name, level){

			// Properties
			this.name = name;
			this.level = level;
			this.health = 2 * level;
			this.attack = level;

			// Methods
			this.tackle = function(target){
				console.log(this.name + ' tackled ' + target.name);
				console.log("Geodude's health is now reduced to " + Number(target.health - this.attack));
			};

			this.faint = function(){
				console.log(this.name + ' fainted. ');
			};
			

		}
			console.log(newPokemon);
			let mewtwo = new Pokemon('Mewtwo', 84);
		    mewtwo.tackle(geodude);
		    geodude.faint();

		let newPokemon2 = [
				{
					name: 'Geodude',
					level: 8,
					health: -84,
					attack: 8,
					tackle: function(){
					console.log('This Pokemon tackled targetPokemon');
					console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
				}
			}
				];

				console.log(newPokemon2);	